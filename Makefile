deploy:
	sed -i 's/{{APP_VERSION}}/'"$(CI_COMMIT_TAG)"'/g' deploy/deploy.yaml
	#kubectl apply -f deploy/deploy.yaml

deploy_local:
	sed 's/{{APP_VERSION}}/v0.1.9/g' deploy/deploy.yaml > deploy_aux.yaml
	kubectl apply -f deploy_aux.yaml
	rm deploy_aux.yaml


.PHONY: deploy
