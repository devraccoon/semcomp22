# SemComp 22

```
People involved 

Pedro Puzzi - pedro.puzzi@raccoon.ag          (Projects Team)
Maria Vitória -  inocencio@raccoon.ag         (Front End)
Lilian - lilian@raccoon.ag                    (Tags Team) - 
Kleber - kleber.yuji@raccoon.ag               (Business Intelligence Team)
André Nascimento - andre.ambrosio@raccoon.ag  (Data Engineering Team)
```

stack: flask/react/redux/mongodb/webpack/docker

# Instructions

```bash
git clone https://gitlab.com/devraccoon/semcomp22.git
```

**Build & run backend instructions**

```bash
python3 -m venv env
pip install -r requirements.txt -r dev_requirements.txt
source env/bin/activate
python app.py
```

**Build react devserver (w/ hot reload)**

```bash
cd ui
npm install
npm run dev
```

**Build docker image**

```bash
docker-compose build
```

**Run the application @ container**

```bash
docker-compose up
```

**Access via http://localhost:5000**

When build, we should now be seeing a simple shopping cart application. When the purchase is done
you can shop again clicking in 'Voltar a loja'

This pieace of software is missing several stuff and probably have a lot of bugs. But the core ideia 
is to make it better by contributing!


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
