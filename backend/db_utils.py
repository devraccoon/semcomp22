from datetime import datetime
import mongoengine as me
from server.database import init_db
from server.config import config
from server.models.product import Product
from server.models.inventory import Inventory
from server.models.cart import Cart
from server.models.user import User

time_now = datetime.now()

my_products = [
    {
        'sku': 'sku100',
        'prod_type': 'doce',
        'title': 'Pudim',
        'description': (
            "Sobremesa de consistência cremosa, a base de chocolate. frutas etc.. "
            "Preparada com leite, leite condensado e ovos e assada cozida em banho maria"
            "em calda de açucar queimado"
        ),
        'pricing': 100

    },
    {
        'sku': 'sku200',
        'prod_type': 'salgado',
        'title': 'Pão',
        'description': (
            "alimento feito com farinha, esp. de trigo, amassada com água e fermento"
            "e assado ao forno."
        ),
        'pricing': 50

    },
    {
        'sku': 'sku300',
        'prod_type': 'salgado',
        'title': 'Esfiha Carne Moida',
        'description': (
            "alimento feito com farinha, esp. de trigo, amassada com água e fermento"
            "e assado ao forno e recheado com carne moida de alta qualidade"
        ),
        'pricing': 10

    },
    {
        'sku': 'sku400',
        'prod_type': 'doce',
        'title': 'Esfiha de Chocolate',
        'description': (
            "alimento feito com farinha, esp. de trigo, amassada com água e fermento"
            "assada e preenchida com delicioso chocolate"
        ),
        'pricing': 10

    },
    {
        'sku': 'sku001',
        'prod_type': 'doce',
        'title': 'Alfajor Turma da Mônica',
        'description': (
            "Conhecido como o alfajor mais gostoso do mundo, esse alimento alimento extinto do planeta "
            "na famosa batalha dos alfajores"
        ),
        'pricing': 10

    },
    {
        'sku': 'sku010',
        'prod_type': 'doce',
        'title': 'Paçoquinha',
        'description': (
            "Paçoca deliciosamente feita do mais puro creme do amendoim"
        ),
        'pricing': 5

    },

]

my_carts = [
    {
        'cart_id': 'cart_id_1',
        'last_modified': time_now,
        'status': 'active',
        'items': []
    },
    {
        'cart_id': 'cart_id_2',
        'last_modified': time_now,
        'status': 'active',
        'items': []
    }
]

my_inventory = [
    {
        '_id': 'sku100',
        'qty': 100,
        'carted': []

    },
    {
        '_id': 'sku200',
        'qty': 100,
        'carted': []

    },
    {
        '_id': 'sku300',
        'qty': 50,
        'carted': []
    },
    {
        '_id': 'sku400',
        'qty': 50,
        'carted': []
    },
    {
        '_id': 'sku001',
        'qty': 50,
        'carted': []
    },
    {
        '_id': 'sku010',
        'qty': 50,
        'carted': []
    },

]

my_users = [
    {
        'sub': '1',
        'name': 'semcomp22',
        'email': 'semcomp22@usp.br',
        'modified': time_now,
    },
]


def reset_db():

    print('Connecting to mongo and building initial data')

    mongo_conn = init_db(config)

    Product.drop_collection()

    for p in my_products:
        product = Product(**p)
        product.save()

    Cart.drop_collection()

    for c in my_carts:
        cart = Cart(**c)
        cart.save()

    Inventory.drop_collection()

    for i in my_inventory:
        inventory = Inventory(**i)
        inventory.save()

    User.drop_collection()

    for u in my_users:
        user = User(**u)
        user.save()

    print('Success')

    print(time_now)
    print(time_now.timestamp())


if __name__ == "__main__":
    reset_db()
