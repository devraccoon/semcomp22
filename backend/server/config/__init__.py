import os

config = {
    "DB": {
        'host': os.getenv("SS_DB_HOST", "localhost"),
        'port': int(os.getenv("SS_DB_PORT", "27017")),
        'db': os.getenv("SS_DB_DATABASE", 'basic_api_data')
    },
    "APP": {
        "SECRET_KEY": os.getenv("SS_SECRET_KEY", "localDevelopment")
    },
    "GTM": os.getenv("SS_GTM", 'GTM-K7ZMZBV')
}
