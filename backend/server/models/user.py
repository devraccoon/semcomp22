import mongoengine as me


class User(me.Document):
    sub = me.IntField(primary_key=True)
    name = me.StringField()
    email = me.StringField()
    modified = me.DateTimeField()
