from flask import Blueprint, render_template, jsonify
from server.models.client import Client
import db_utils


def create_db_blueprint(debug):
    db_blueprint = Blueprint("db_blueprint", __name__)

    @db_blueprint.route("/clear")
    def cleanDB():
        db_utils.reset_db()
        return "", 200

    return db_blueprint
