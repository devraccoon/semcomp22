import logging
from functools import wraps
from flask import Blueprint
import json


def check_successful(service_name=''):
    def outer_wrapper(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            status = True

            try:
                func(*args, **kwargs)
            except Exception:
                logging.exception('Error while checking {} health'.format(
                    service_name))
                status = False

            return {service_name: status}
        return wrapper
    return outer_wrapper


def create_healthcheck_blueprint(debug, db_client):
    healthcheck_blueprint = Blueprint('healthcheck_blueprint', __name__)

    @healthcheck_blueprint.route('/', methods=['GET'])
    def healthcheck():
        @check_successful('database')
        def check_db():
            db_client.admin.command('ismaster')

        services = {**check_db()}

        healthy = all(services.values())
        status = 200 if healthy else 500

        return json.dumps({
            'healthy': healthy,
            'services': services
        })

    return healthcheck_blueprint
