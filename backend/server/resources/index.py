from datetime import datetime
from flask import Blueprint, render_template


def create_index_blueprint(debug, session, gtm):
    index_blueprint = Blueprint("index_blueprint", __name__)

    @index_blueprint.route("/")
    def index():

        default_user = {
            'sub': '1',
            'name': "semcomp22",
            'email': "semcomp22@usp.br",
            'cart_id': 'cart_id_1',
            'modified': datetime.now(),
        }

        session.clear()
        session['cart'] = default_user['cart_id']
        session['user'] = default_user['name']

        return render_template("index.html", user=default_user, debug=debug, session=session, gtm=gtm)

    return index_blueprint
