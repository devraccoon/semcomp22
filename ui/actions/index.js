import { pushDataLayer } from "../src/Components/utils";

export const fetchProducts = () => dispatch => {
  dispatch({
    type: "FETCH_PRODUCTS",
    payload: {
      request: {
        method: "GET",
        url: "/inventory/details"
      },
      options: {
        onSuccess: ({ response }) => {
          pushDataLayer({ event: "VirtualPageView", data: response.data, path: "/product" });
          dispatch({
            type: "FETCH_PRODUCTS_SUCCESS",
            payload: {
              data: response.data
            }
          });
        }
      }
    }
  });
};

export const fetchCart = () => dispatch => {
  dispatch({
    type: "FETCH_CART",
    payload: {
      request: {
        method: "GET",
        url: "/cart/cart_id_1"
      },
      options: {
        onSuccess: ({ response }) => {
          pushDataLayer({
            event: "VirtualPageView",
            data: response.data,
            path: `/cart`,
            type: "cart"
          });
          dispatch({
            type: "FETCH_CART_SUCCESS",
            payload: {
              data: response.data
            }
          });
        }
      }
    }
  });
};

export const fetchCartStatus = (cart_id = 'cart_id_1') => dispatch => {
  dispatch({
    type: "FETCH_CART_STATUS",
    payload: {
      request: {
        method: "GET",
        url: `/cart/${cart_id}`
      },
    }
  })
}

export const addProductToCart = (productID, quantity, cart_id = "cart_id_1") => ({
  type: "ADD_ITEM_TO_CART",
  payload: {
    request: {
      method: "POST",
      url: `/cart/${cart_id}/${productID}/${quantity}/details_default`
    }
  }
});

export const checkoutCart = (cart_id = 'cart_id_1') => ({
  type: "CART_CHECKOUT",
  payload: {
    request: {
      method: "POST",
      url: `/cart/${cart_id}/checkout`
    }
  }
});

export const clearCart = (cart_id = "cart_id_1") => ({
  type: "CART_CLEAR",
  payload: {
    request: {
      method: "POST",
      url: `/cart/${cart_id}/clear`
    }
  }
});

export const deleteProductCart = (productID, quantity, timestamp, cart_id = "cart_id_1") => ({
  type: "DELETE_ITEM_CART",
  payload: {
    request: {
      method: "DELETE",
      url: `/cart/${cart_id}/${productID}/${quantity}/${timestamp}`
    }
  }
});

export const setProductToDisplay = product => ({
  type: "DISPLAY_PRODUCT",
  payload: product
});

export const setViewDisplay = viewing => ({
  type: "SET_MAIN_VIEW",
  payload: viewing
});

