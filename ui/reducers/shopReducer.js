import {
  FETCH_PRODUCTS,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_CART,
  FETCH_CART_SUCCESS,
  CART_CHECKOUT_SUCCESS
} from "../actions/action-types";

const initialState = {
  products: [],
  cart: {
    status: "active",
    items: []
  },
  viewing: "items",
  checkout: {
    name: "",
    surname: "",
    cpf: "",
    address: "",
    credit_card: "",
    payment_slip: ""
  }
};

const shopReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS: {
      return {
        ...state
      };
    }
    case FETCH_PRODUCTS_SUCCESS: {
      return {
        ...state,
        products: [...action.payload.data]
      };
    }
    case FETCH_CART: {
      return {
        ...state
      };
    }
    case FETCH_CART_SUCCESS: {
      return {
        ...state,
        cart: { ...action.payload.data }
      };
    }
    case "FETCH_CART_STATUS_SUCCESS": {
      return {
        ...state,
        cart: { ...action.payload.data }
      };
    }
    case "ADD_ITEM_TO_CART_SUCCESS": {
      const { products } = state;
      const productIndex = products.findIndex(product => product._id === action.payload.data._id);
      const productsInStock = action.payload.data.qty;

      return {
        ...state,
        products: [
          ...products.slice(0, productIndex),
          { ...products[productIndex], qty: productsInStock },
          ...products.slice(productIndex + 1)
        ],
        displayProduct: { ...state.displayProduct, qty: productsInStock }
      };
    }
    case CART_CHECKOUT_SUCCESS: {
      return {
        ...state,
        cart: { ...action.payload.data }
      };
    }

    case 'CART_CLEAR_SUCCESS': {

      const { cart } = state

      return {
        ...state,
        cart: {
          ...cart,
          status: 'active'
        },
        viewing: "items",
      }
    }

    case "DISPLAY_PRODUCT": {
      return {
        ...state,
        displayProduct: { ...action.payload }
      };
    }
    case "DELETE_ITEM_CART_SUCCESS": {
      const { items } = state.cart;
      const { data } = action.payload;
      const newItems = items.filter(
        i => !(i.sku === data.sku && i.timestamp.$date === data.timestamp)
      );
      return { ...state, cart: { ...state.cart, items: newItems } };
    }
    case "SET_MAIN_VIEW": {
      return { ...state, viewing: action.payload };
    }
    default:
      return state;
  }
};

export default shopReducer;
