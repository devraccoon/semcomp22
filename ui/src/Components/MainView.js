import React, { useEffect } from "react";
import { Layout } from "antd";
import ItemsComponent from "./Shop/ItemComponent";
import ShopHeader from "./Shop/ShopHeader";
import ShopFooter from "./Shop/ShopFooter";
import ShoppingCart from "./Shop/ShoppingCart";
import CheckoutSteps from "./Shop/CheckoutSteps";
import { connect } from "react-redux";
import { setViewDisplay as setViewDisplayAction } from "../../actions/index";
import { fetchCartStatus as fetchCartStatusAction } from "../../actions/index";
import { clearCart as clearCartAction } from "../../actions/index";

const contentStyle = { padding: "0 50px", marginTop: 64 };

const contentDivStyle = {
  display: 'flex',
  backgroundColor: "white",
  padding: "24px",
  height: "700px",
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center'

};

const PurchaseComplete = ({ clearCart, cart }) => {
  return (
    <Layout.Content style={contentStyle}>
      <div style={contentDivStyle}>
        <h1 style={{ textAlign: "center" }}>Compra Encerrada</h1>
        <button style={{ width: '200px', height: '50px', marginTop: '30px', color: 'white', backgroundColor: '#004f78' }} onClick={() => clearCart(cart.cart_id)}>Voltar a loja</button>
      </div>
    </Layout.Content >
  );
};

const getMainViewRoutes = (clearCart, cart) => {
  return {
    items: <ItemsComponent />,
    cart: <ShoppingCart />,
    checkout: <CheckoutSteps />,
    complete: <PurchaseComplete clearCart={clearCart} cart={cart} />
  }
};


const MainView = ({ viewing, setDisplay, fetchCartStatus, cart, clearCart }) => {
  useEffect(() => {
    fetchCartStatus();
  }, []);

  const routes = getMainViewRoutes(clearCart, cart)

  return (
    <Layout>
      <ShopHeader cartStatus={cart.status} menuState={viewing} setMenu={setDisplay} />
      <div>{cart.status === "active" ? routes[viewing] : routes[cart.status]}</div>
    </Layout>
  );
};

const mapStateToProps = state => ({
  viewing: state.shop.viewing,
  cart: state.shop.cart
});

const mapDispatchToProps = {
  setDisplay: setViewDisplayAction,
  fetchCartStatus: fetchCartStatusAction,
  clearCart: clearCartAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainView);
