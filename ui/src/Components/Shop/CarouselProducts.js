import { Carousel } from "antd";
import React from "react";

const StyleSlider = {
    textAlign: "center",
    height: "160px",
    lineHeight: "160px",
    background: "#364d79",
    overflow: "hidden"
};

const carrouselSettings = {
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    dots: true,
    infinite: true
};

const StyleH3 = {
    border: "3px solid cyan",
    height: "100%",
    padding: "0 0 0 0",
    margin: "0 0 0 0"
};


const CarouselProductsList = (products) => {
    products.map((product, idx) => (
        <div color="cyan" key={idx}>
            <h3 style={StyleH3} onClick={() => handeClick(product)}>
                {product._id}
            </h3>
        </div>
    ))
}


const CarouselComponent = () => {
    <Carousel style={StyleSlider} {...carrouselSettings}>
        <ProductsList />
    </Carousel>
}

export default CarouselComponent