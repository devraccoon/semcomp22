import React from "react";
import { connect } from "react-redux";
import { deleteProductCart as deleteProductCartAction } from "../../../actions/index";
import { pushDataLayer } from "../utils";

const flexContainer = {
  padding: "0",
  margin: "0",
  listStyle: "none",
  display: "flex",
  flexGrow: "1"
};

const flexItem = {
  background: " tomato",
  padding: " 5px",
  width: " 60px",
  height: " 50px",
  margin: " 5px",
  lineHeight: " 50px",
  color: " white",
  fontWeight: " bold",
  fontSize: "20px",
  textAlign: " center"
};

const flexItem2 = {
  background: " tomato",
  padding: " 5px",
  width: " 60px",
  height: " 50px",
  margin: " 5px",
  color: " white",
  fontWeight: " bold",
  fontSize: "20px",
  textAlign: " center",
  flexGrow: "1"
};

const divStyle = {
  display: "flex",
  flexDirection: "row",
  width: '70%',
};

const onClickDelete = (sku, qty, timestamp, deleteProduct) => {
  pushDataLayer({
    event: "RemoveItemFromCart",
    type: "RemoveItem",
    sku: sku,
    qty: qty,
    timestamp: timestamp
  });
  deleteProduct(sku, qty, timestamp);
};

const CartItem = ({ item, deleteProduct }) => {

  const { details } = item
  return (
    <div style={flexContainer}>
      <div style={flexItem2}>
        {details.title} [qty={item.qty}, total={details.pricing * item.qty}]
      </div>
      <button style={flexItem} onClick={() => onClickDelete(item.sku, item.qty, item.timestamp.$date, deleteProduct)}>x</button>
    </div>
  )
}

const CartProductList = ({ cartItems, deleteProduct }) => {
  const generateKey = (pre) => { return `${pre}_${new Date().getTime()}` }
  return (
    <div style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '36px' }}>
      {
        cartItems.map(
          (i, index) =>
            <div key={generateKey(index)} style={divStyle}>
              <CartItem item={i} deleteProduct={deleteProduct} />
            </div >
        )
      }
    </div>
  )
};

const mapStateToProps = state => ({
  cartItems: state.shop.cart.items
});

const mapDispatchToProps = {
  deleteProduct: deleteProductCartAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartProductList);
