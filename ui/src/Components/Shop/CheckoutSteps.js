import React, { useState } from "react";
import { connect } from "react-redux";
import styles from "./checkout.less";
import { Layout } from "antd";
import { Steps, Button, message } from "antd";
import { checkoutCart as checkoutCartAction } from "../../../actions/index";
import useInput from "../Shop/hooks";
import { pushDataLayer } from "../utils";
import { Radio, Input } from "antd";

const stepsContent = {
  display: "flex",
  flexDirection: "column",
  background: "#e4e4e4",
  height: "300px",
  textAlign: "center",
  marginTop: "10px",
  alignItems: "center",
  justifyContent: "space-between"
};

const contentStyle = { padding: "0 200px 0 200px", marginTop: 64, display: 'flex', justifyContent: 'center' };

const contentDivStyle = {
  backgroundColor: "#white",
  padding: "50px",
  height: "500px",
  display: 'flex',
  flexDirection: 'column'
};

const divContentStyle = {
  display: "flex",
  flexDirection: "column",
  marginTop: "50px"
};

const radioStyle = {
  display: "block",
  height: "30px",
  lineHeight: "30px"
};

const steps = ["Dados Pessoais", "Entrega", "Pagamento"];

const CheckoutSteps = ({ purchaseClick, cart }) => {
  const { Content } = Layout;
  const { Step } = Steps;

  const [current, setCurrent] = useState(0);

  const inputName = useInput();
  const inputCpf = useInput();

  const [stepTwoOption, setStepTwo] = useState(0);
  const [stepThreeOption, setStepThree] = useState(0);

  const stepTwoOptionsList = ["Retirada", "Sedex", "Frete Gratis"];
  const stepThreeOptionsList = ["Boleto", "Cartão"];

  const onClickPurchase = () => {
    purchaseClick();

    pushDataLayer({
      event: "checkoutOption1",
      option: stepThreeOptionsList[stepThreeOption - 1]
    });

    pushDataLayer({
      event: "VirtualPageView",
      path: "/purchase",
      data: {
        cart_id: cart._id,
        items: cart.items,
        option: stepThreeOptionsList[stepThreeOption - 1]
      }
    });
    message.success("Processing complete!");
  };

  const stepNext = () => {
    switch (current) {
      case 0:
        setCurrent(1);

        pushDataLayer({
          event: "checkoutStep0",
          payload: { cart_id: cart._id, items: cart.items }
        });

        break;
      case 1:
        pushDataLayer({
          event: "checkoutOption0",
          option: stepTwoOptionsList[stepTwoOption - 1]
        });

        setCurrent(2);

        pushDataLayer({
          event: "checkoutStep1",
          payload: { cart_id: cart._id, items: cart.items }
        });

        break;
      default:
        console.log("current do stepNext", current);
    }
  };

  const stepPrevious = () => {
    switch (current) {
      case 1:
        setCurrent(0);
        pushDataLayer({ event: "checkoutStepBack0" });
        break;
      case 2:
        setCurrent(1);
        pushDataLayer({ event: "checkoutStepBack1" });
        break;
    }
  };

  const nextButton = () => (
    <Button type="primary" onClick={() => stepNext()}>
      Proximo
    </Button>
  );

  const previousButton = () => (
    <Button style={{ marginLeft: 8 }} onClick={() => stepPrevious()}>
      Anterior
    </Button>
  );
  const PurchaseButton = () => {
    return (
      <>
        <Button type="primary" onClick={() => onClickPurchase()}>
          Finalizar
        </Button>
      </>
    );
  };

  const personalData = () => {
    return (
      <>
        Nome <input {...inputName} /> CPF <input {...inputCpf} />
      </>
    );
  };

  const deliveryMethod = () => {
    return (
      <div style={{ display: "flex", textAlign: "justify" }}>
        <Radio.Group onChange={e => setStepTwo(e.target.value)} value={stepTwoOption}>
          <Radio style={radioStyle} value={1}>
            Retirar Loja
          </Radio>
          <Radio style={radioStyle} value={2}>
            Sedex
          </Radio>
          <Radio style={radioStyle} value={3}>
            Frete Grátis
          </Radio>
        </Radio.Group>
      </div>
    );
  };

  const paymentMethod = () => {
    return (
      <div style={{ display: "flex", textAlign: "justify" }}>
        <Radio.Group onChange={e => setStepThree(e.target.value)} value={stepThreeOption}>
          <Radio style={radioStyle} value={1}>
            Boleto
          </Radio>
          <Radio style={radioStyle} value={2}>
            Cartão
          </Radio>
        </Radio.Group>
      </div>
    );
  };

  const stepperComponents = [personalData, deliveryMethod, paymentMethod];

  return (
    <Content style={contentStyle}>
      <div style={contentDivStyle}>
        <Steps className={styles["ant-steps-item-icon"]} current={current}>
          {steps.map(item => (
            <Step key={item} title={item} />
          ))}
        </Steps>

        <div style={stepsContent}>
          <div style={divContentStyle}>{stepperComponents[current]()}</div>

          <div style={{ marginBottom: "10px" }}>
            {current < steps.length - 1 && nextButton()}
            {current === steps.length - 1 && <PurchaseButton />}
            {current > 0 && previousButton()}
          </div>
        </div>
      </div>
    </Content>
  );
};

const mapStateToProps = state => ({
  cart: state.shop.cart
});

const mapDispatchToProps = {
  purchaseClick: checkoutCartAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutSteps);
