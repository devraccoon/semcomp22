import { Carousel, Layout } from "antd";
import React, { useEffect } from "react";
import { pushDataLayer } from "../utils";
import { connect } from "react-redux";
import {
  fetchProducts as fetchProductsAction,
  addProductToCart as addProductToCartAction,
  setProductToDisplay
} from "../../../actions/index";
import ProductDisplay from "./ProductDisplay";

const contentStyle = {
  marginTop: "80px",
};


const allProductsDivStyle = {
  backgroundColor: "#e4e4e4",
  height: "300px",
  widht: "100%",
  padding: '50px 150px 50px 150px',
};

const sliderStyle = {
  textAlign: "center",
  overflow: "hidden",
};

const divH3 = {
  height: '200px',
  width: '200px',
  backgroundColor: 'orange'

}

const StyleH3 = {
  height: "100%",
  background: '#82b5d0',
  marginRight: '24px',
};

const carrouselSettings = {
  dots: true,
  slidesToShow: 5,
  slidesToScroll: 1,
  infinite: true,
  draggable: true,
  arrows: true
};

const ItemsComponent = props => {
  const { fetchProducts, products, setProduct } = props;
  const { Content } = Layout;

  const handeClickProduct = product => {
    setProduct(product);
    pushDataLayer({ event: "productClick", sku: product._id });
    pushDataLayer({ event: "VirtualPageView", sku: product._id, path: `/product/${product._id}` });
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <Content style={contentStyle}>
      <ProductDisplay />
      <div style={allProductsDivStyle}>
        <Carousel style={sliderStyle} {...carrouselSettings}>
          {products.map((product, idx) => (
            <div key={idx}>
              <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }} onClick={() => handeClickProduct(product)}>
                <h3 style={{ margin: 0, background: '#b7abab', marginRight: '24px', height: '200px' }}>Foto</h3>
                <h3 style={StyleH3}>{product.title}</h3>

              </div>
            </div>
          ))}
        </Carousel>
      </div>
    </Content>
  );
};

const mapStateToProps = state => ({
  products: state.shop.products
});

const mapDispatchToProps = {
  fetchProducts: fetchProductsAction,
  addToCart: addProductToCartAction,
  setProduct: setProductToDisplay
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemsComponent);
