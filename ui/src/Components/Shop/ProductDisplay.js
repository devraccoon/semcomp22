import React, { useState } from "react";
import { connect } from "react-redux";
import { setProductToDisplay, addProductToCart } from "../../../actions/index";
import { pushDataLayer } from "../utils";

const contentDivStyle = () => {
  return {
    height: "400px",
    background: 'white'
  };
};

const productDisplayStyle = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  padding: '50px 50px'
}

const imageProductStyle = {
  height: '250px',
  width: '350px',
  background: 'orange',
  marginRight: '48px',

}

const productInfo = {
  display: 'flex',
  flexDirection: 'column',
  height: '250px',
  width: '700px',
  justifyContent: 'space-between',
}

const handeClickAdd = (product, quantity, addToCart) => {
  pushDataLayer({ event: "addItemToCart", type: "addItem", sku: product._id, qty: quantity });
  addToCart(product["_id"], quantity);
};


export const ProductDisplay = ({ product, addToCart }) => {
  const [quantity, setQuantity] = useState(0);

  return (
    <div style={contentDivStyle()}>
      {product && (
        <div style={productDisplayStyle}>
          <div style={imageProductStyle}>
            <p>Foto</p>
          </div>
          <div style={productInfo}>
            <div>
              <div><h1 style={{ fontSize: '2em', fontFamily: 'sans-serif' }} >{product.title}</h1></div>
              <div><strong><p>Preço R${product.pricing + ',00'}</p></strong><p>ou 2x de <strong>{(product.pricing / 2) + ',00'}</strong></p></div>
              <div><p style={{ fontSize: '16px', fontFamily: 'sans-serif' }}>Descrição {product.description}</p></div>
            </div>
            <div style={{ marginBottom: '12px' }}>
              <p style={{ color: '#8e919c' }}>{product.qty} em estoque</p>
              <input value={quantity} onChange={e => setQuantity(e.target.value)} />
              <button onClick={() => handeClickAdd(product, quantity, addToCart)}>
                Adicionar ao carrinho
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};




const mapStateToProps = state => ({
  product: state.shop.displayProduct
});

const mapDispatchToProps = {
  setProduct: setProductToDisplay,
  addToCart: addProductToCart
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDisplay);
