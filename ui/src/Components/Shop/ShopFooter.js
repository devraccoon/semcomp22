import React from "react";
import { Layout } from "antd";

const footerStyle = {
  textAlign: "center"
};

const ShopFooter = () => {
  const { Footer } = Layout;

  return (
    <Footer style={footerStyle}>Project powered by React, Ant Design and RACCOON IT TEAM </Footer>
  );
};

export default ShopFooter;
