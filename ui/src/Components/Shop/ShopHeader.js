import React from "react";
import { Layout, Menu } from "antd";

const styleHeader = {
  zIndex: "2",
  width: "100%",
  color: "#ffffff",
  position: "fixed",
  backgroundColor: "#004f78",
  height: "80px",
  padding: 0,
  margin: 0,
  display: "flex",
  flexDirection: "column-reverse"
};

const menuProps = {
  style: {
    margin: 0,
    padding: 0,
    width: "100%",
    lineHeight: "45px",
    background: "#4c83a0",
    display: "flex",
  },
  theme: "dark",
  mode: "horizontal",
  defaultSelectedKeys: ["items"]
};

const ShopHeader = ({ menuState, setMenu, cartStatus }) => {
  const { Header } = Layout;

  const onClickActions = key => {
    setMenu(key);
  };

  return (
    <Header style={styleHeader}>
      <Menu {...menuProps} onClick={e => onClickActions(e.key)} selectedKeys={[menuState]}>
        {cartStatus === "active" && <Menu.Item key="items">items</Menu.Item>}
        {cartStatus === "active" && <Menu.Item key="cart">cart</Menu.Item>}
        {menuState === "checkout" && cartStatus === "active" && (
          <Menu.Item key="checkout">checkout</Menu.Item>
        )}
      </Menu>
    </Header>
  );
};

export default ShopHeader;
