import React, { useEffect } from "react";
import { Layout } from "antd";
import { pushDataLayer } from "../utils";
import { connect } from "react-redux";
import {
  fetchCart as fetchCartAction,
  checkoutCart as checkoutCartAction,
  setViewDisplay as setViewDisplayAction
} from "../../../actions";

import CartProductList from "./CartProductList";

const contentDivStyle = () => {
  return {
    display: 'flex',
    flexDirection: 'column',
    textAlign: "center",
    backgroundColor: "white",
    padding: 0,
    height: "710px",
    justifyContent: 'space-between'
  };
};

const contentStyle = {
  marginTop: 64
};

const ShoppingCart = ({ fetchCart, cart, setDisplay }) => {
  const { Content } = Layout;

  const onClickCheckout = (cart) => {
    pushDataLayer({ event: "VirtualPageView", payload: cart, path: '/checkout' });
    setDisplay('checkout')
  };

  const calculatePurchaseTotal = (cart) => {
    return cart.items.reduce((accumulatedValue, item) => accumulatedValue + (item.qty * item.details.pricing), 0)
  }

  useEffect(() => {
    fetchCart();
  }, []);

  return (
    <Content style={contentStyle}>
      <div style={contentDivStyle()}>
        <CartProductList />
        <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '0', backgroundColor: '#4c83a0', height: '50px', alignItems: 'center' }}>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <h1 style={{ marginBottom: 0, marginLeft: '24px', color: 'white' }}>Status:{cart.status}</h1>
            <h1 style={{ marginBottom: 0, marginLeft: '24px', color: 'white' }}>Total:{calculatePurchaseTotal(cart)}</h1>
          </div>
          <button style={{ marginRight: '24px' }} onClick={() => onClickCheckout(cart)}>go to checkout</button>
        </div>
      </div>
    </Content >
  );
};

const mapStateToProps = state => ({
  cart: state.shop.cart
});

const mapDispatchToProps = {
  fetchCart: fetchCartAction,
  checkoutCart: checkoutCartAction,
  setDisplay: setViewDisplayAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoppingCart);




