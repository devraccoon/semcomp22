import React from "react";
import { connect } from "react-redux";
import useInput from "../Shop/hooks";

const divStyle = {
  display: "flex",
  flexDirection: "column",
  marginTop: "24px",
  width: "50%"
};

const personalData = () => {
  const inputName = useInput();
  const inputSurName = useInput();
  const inputCpf = useInput();

  return (
    <>
      Nome <input {...inputName} /> Sobrenome <input {...inputSurName} /> CPF
      <input {...inputCpf} />
    </>
  );
};
const deliveryMethod = () => {
  const inputEnd = useInput();
  const inputCpf = useInput();

  return (
    <>
      <input {...inputEnd} />
      <input {...inputCpf} />
    </>
  );
};

const PaymentMethod = () => {
  const inputCreditCard = useInput();
  const inputSlip = useInput();
  return (
    <>
      Cartão de crédito: <input {...inputCreditCard} />
      Boleto: <input {...inputSlip} />
    </>
  );
};

export const Stepper = ({ current }) => {
  const stepperComponentMap = [personalData, deliveryMethod, PaymentMethod];
  return <div style={divStyle}>{stepperComponentMap[current]()}</div>;
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Stepper);
