import { createStore, applyMiddleware } from "redux";
import axios from "axios";
import axiosMiddleware from "redux-axios-middleware";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "../reducers/index";
import thunk from "redux-thunk";

const api = axios.create({
  baseURL: "/api",
  responseType: "json"
});

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk, axiosMiddleware(api)))
);

export default store;
